package a1.apiandrecycleviewexample;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    RecyclerView recyclerView;
    ProgressBar progressBar;

    Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;
        recyclerView = (RecyclerView) findViewById(R.id.recycleView);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));

        //membuat data gempa dummy
        List<Gempa> gempaList = new ArrayList<>(); //instansiasi objek baru linkedlist dengan model Gempa
        //gempalist di sini tidak mempunyai elemen apapun bisa dibuktikan dengan gempaList.size() itu return 0

        //memasukan data keobjek baru melalui constructor
        Gempa gempa = new Gempa("tanggal","waktu","lintang bujur","magnitudo","kedalaman",
                "lokasi","dirasakan","http:\\/\\/inatews.bmkg.go.id\\/shakemap\\/20161111063427\\/download\\/intensity.jpg");

        //memasukkan data ke objek baru melalui encapsulation
        Gempa gempa1 = new Gempa();
        gempa1.setTgl("tanggal2");
        gempa1.setWaktu("waktu2");
        gempa1.setLintang_bujur("lintang bujur2");
        gempa1.setMagnitudo("magnitudo2");
        gempa1.setKedalaman("kedalaman2");
        gempa1.setLokasi("lokasi2");
        gempa1.setDirasakan("dirasakan2");
        gempa1.setImg("http:\\/\\/inatews.bmkg.go.id\\/shakemap\\/20161110142321\\/download\\/intensity.jpg");

        //memasukkan objek2 itu kedalam linkedlist
        gempaList.add(gempa);
        gempaList.add(gempa1);

        //menampilkannya ke recycleview melalui prosedur yang sudah dibuat
        setrecycleview(gempaList);

//        mengambil data gempa dari API
//        GetDataGempa();

    }

    private void loading(boolean status){
        progressBar.setVisibility(status ? View.VISIBLE : View.GONE);
        recyclerView.setVisibility(status ? View.GONE : View.VISIBLE);
    }

    private void GetDataGempa(){
        loading(true);

        ApiService apiServices = ApiService.Factory.create(mContext);
        Call<Data> getData = apiServices.getDataGempa("gempa-terkini","97c4576c7a1a5c0ba040c4e8d1b27f78");

        getData.enqueue(new Callback<Data>() {
            @Override
            public void onResponse(Call<Data> call, Response<Data> response) {
                Data resData = response.body();

                if(resData.getStatus().equals("success")){
                    setrecycleview(resData.getData());
                }else{
                   Toast.makeText(mContext,resData.getMessage(),Toast.LENGTH_LONG).show();
                }
                loading(false);
            }

            @Override
            public void onFailure(Call<Data> call, Throwable t) {
                Toast.makeText(mContext,t.getMessage(),Toast.LENGTH_LONG).show();
                loading(false);
            }
        });
    }

    private void setrecycleview(List<Gempa> gempaList){
        ReycleViewAdapter adapter = new ReycleViewAdapter<Gempa, DataGempaHolder>(R.layout.list_item_data_gempa,
                DataGempaHolder.class, Gempa.class, gempaList) {
            @Override
            protected void bindView(DataGempaHolder holder, Gempa model, final int position) {
                holder.bind(mContext,model);
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(mContext,"Data ke " + position+ " clicked!",Toast.LENGTH_LONG).show();
                    }
                });
            }
        };
        recyclerView.setAdapter(adapter);
    }
}
